package com.shortnotion.androidwifiinterface;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class UnityPlugin {

    // SUB-TYPES

    public interface ConnectionCallback {
        public void OnResult(boolean enabled, boolean connected, String ssid);
    }

    public interface NetworksCallback {
        public void OnResult(String[] configuredSsids, String[] unconfiguredSsids);
    }

    private static final String LOGTAG = "AndroidWifiInterface";
    private final int REQUEST_CODE_LOCATION = 2;
    private final Activity unityActivity;

    // INSTANTIATION

    public static UnityPlugin create(Activity activity) {
        return new UnityPlugin(activity);
    }

    private UnityPlugin(Activity activity) {
        unityActivity = activity;

        if (!checkPermissions()) {
            Log.w(LOGTAG, "checkPermissions returned false");
        }

        Log.i(LOGTAG, "com.shortnotion.androidwifiinterface.UnityPlugin instantiated");
    }

    private boolean checkPermission(String permission, Integer request_code) {
        Log.d(LOGTAG, "Checking permission: "+permission);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            Log.w(LOGTAG, "SDK version < 23, can't verify permissions");
            return true;
        }

        if(unityActivity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }

        Log.w(LOGTAG, "Missing permission: "+permission);

        if (unityActivity.shouldShowRequestPermissionRationale(permission)) {
            Log.w(LOGTAG, "shouldShowRequestPermissionRationale for permission: " + permission);
            return false;
        }

        if (request_code == null) {
            Log.w(LOGTAG, "No request code to request permission: "+permission);
            return false;
        }

        Log.i(LOGTAG, "Requesting permission: "+permission);
        unityActivity.requestPermissions(new String[]{ permission }, request_code);
        return false;
    }

    // PUBLIC API

    public boolean checkPermissions() {
        return
                checkPermission(Manifest.permission.ACCESS_WIFI_STATE, null)
                && checkPermission(Manifest.permission.CHANGE_WIFI_STATE, null)
//                && checkPermission(Manifest.permission.ACCESS_COARSE_LOCATION, REQUEST_CODE_LOCATION);
                && checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, REQUEST_CODE_LOCATION);
    }

    public void GetAvailableNetworks(NetworksCallback callback) {
        Log.d(LOGTAG, "UnityPlugin.GetAvailableNetworks()");
        Scanner.getNetworkNames(unityActivity, (String[] networksNames) -> {

            // check permission (required)
            if (!checkPermission(Manifest.permission.ACCESS_WIFI_STATE, null)) {
                Log.w(LOGTAG, "Permission failure; can't fetch available wifi networks");
                callback.OnResult(new String[]{}, new String[] { });
                return;
            }

            List<String> ssids = new ArrayList<>();
            for (String name : networksNames) {
                String ssid = name.trim();
                if (!ssids.contains(ssid))
                    ssids.add(ssid);
            }

            List<String> configured = new ArrayList<>();
            List<String> unknown = new ArrayList<>();

            List<String> configuredSsids = new ArrayList<>();

            for (WifiConfiguration config : ((WifiManager) unityActivity.getApplicationContext()
                    .getSystemService(Context.WIFI_SERVICE)).getConfiguredNetworks()) {
                configuredSsids.add(config.SSID);
            }

            for(String ssid : ssids) {
                if (configuredSsids.contains(ssid) || configuredSsids.contains("\""+ssid+"\"")) {
                    configured.add(ssid);
                } else {
                    unknown.add(ssid);
                }
            }

            callback.OnResult(configured.toArray(new String[]{}), unknown.toArray(new String[]{}));
        }, null);
    }

    public void GetConnectionInfo(ConnectionCallback callback) {
        WifiManager wifiManager = (WifiManager) unityActivity
                .getApplicationContext()
                .getSystemService(Context.WIFI_SERVICE);

        boolean isEnabled = wifiManager.isWifiEnabled();
        boolean isConnected = ((ConnectivityManager) unityActivity
                .getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE))
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                        .isConnected();
        
        String ssid = wifiManager.getConnectionInfo().getSSID();

        callback.OnResult(isEnabled, isConnected, ssid);
    }

    /**
     * ConfigureNetwork attempts to an available network to the list
     * of configured networks and attempts to connect to it.
     * @param ssid the SSID of the network to configure
     * @param password the password for the network
     */
    public void ConfigureNetwork(String ssid, String password) {
        WifiManager wifi = (WifiManager)unityActivity.getApplicationContext()
                .getSystemService(Context.WIFI_SERVICE);

        // already connected?
        String strQuotationSSID = "\"" + ssid + "\"";
        String strQuotationPSW = "\"" + password + "\"";

        WifiConfiguration config = new WifiConfiguration();
        config.SSID = strQuotationSSID;
        config.preSharedKey = strQuotationPSW;
        config.status = 2; // 0 == Current, 1 == Disabled, 2 == Enable

        Log.i(LOGTAG, "Configuring network with SSID: "+ssid);
        wifi.enableNetwork(wifi.addNetwork(config), true);
    }

    public void RemoveNetwork(String ssid) {
        if (!checkPermission(Manifest.permission.ACCESS_WIFI_STATE, null)) {
            Log.w(LOGTAG, "Permission failure; can't remove configured wifi network: "+ssid);
            return;
        }

        String strQuotationSSID = "\"" + ssid + "\"";

        WifiManager wifi = (WifiManager)unityActivity.getApplicationContext()
                .getSystemService(Context.WIFI_SERVICE);

        for(WifiConfiguration config : wifi.getConfiguredNetworks()) {
            if (config.SSID.equals(ssid) || config.SSID.equals(strQuotationSSID)) {
                Log.i(LOGTAG, "Removing network with SSID: "+ssid);
                wifi.removeNetwork(config.networkId);
                return;
            }
        }

        Log.w(LOGTAG, "Could not find SSID to remove: "+ssid);
    }

    public void GetConfiguredNetworks(NetworksCallback callback) {
        if (!checkPermission(Manifest.permission.ACCESS_WIFI_STATE, null)) {
            Log.w(LOGTAG, "Permission failure; can't fetch configured wifi networks");
            callback.OnResult(new String[]{}, new String[] { });
            return;
        }

        List<String> configuredSsids = new ArrayList<>();

        for (WifiConfiguration config : ((WifiManager) unityActivity.getApplicationContext()
                .getSystemService(Context.WIFI_SERVICE)).getConfiguredNetworks()) {
            configuredSsids.add(config.SSID);
        }

        callback.OnResult(configuredSsids.toArray(new String[]{ }), new String[] { });
    }

    /**
     * Connect attempts to connect to the specified network
     * @param ssid the SSID of the network.
     */
    public void Connect(String ssid) {
        WifiManager wifi = (WifiManager) unityActivity.getApplicationContext()
                .getSystemService(Context.WIFI_SERVICE);

        // already connected?
        String strQuotationSSID = "\"" + ssid + "\"";
        WifiInfo wifiInfo = wifi.getConnectionInfo();
        if (wifiInfo != null && (ssid.equals(wifiInfo.getSSID()) || strQuotationSSID.equals(wifiInfo.getSSID()))) {
            Log.w(LOGTAG, "Already connected to network: " + ssid);
            return;
        }

        if (!checkPermission(Manifest.permission.ACCESS_WIFI_STATE, -1)) {
            Log.w(LOGTAG, "Permission failure; can't fetch configured wifi networks");
            return;
        }

        List<WifiConfiguration> configurations = wifi.getConfiguredNetworks();
        if (configurations == null) {
            Log.w(LOGTAG, "No networks found for connecting to: " + ssid);
            return;
        }

        for(WifiConfiguration config : configurations) {
            if (!config.SSID.equals(ssid)) continue;

            // connect
            Log.i(LOGTAG, "Attempting to connect to network with SSID: " + ssid);
            wifi.enableNetwork(config.networkId, true);
        }

        // network not found
        Log.w(LOGTAG, "Could not find network to connect to: " + ssid);
    }
}