package com.shortnotion.androidwifiinterface;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

class Scanner {
    public interface NetworkNamesCallback {
        public void OnResult(String[] names);
    }

    private static final String LOGTAG = "Scanner";

    private final Activity unityActivity;
    private final WifiManager wifiManager;
    private final NetworkNamesCallback callback;
    private final Runnable failureCallback;
    private final BroadcastReceiver receiver;

    public static void getNetworkNames(Activity activity, NetworkNamesCallback callback, Runnable failureCallback) {
        // onScanSuccess will invoke callback when results of wifi scan are available
        Scanner scanner = new Scanner(activity, callback, failureCallback);

        // start wifi scan which should ultimately invoke the onScanSuccess asynchronously
        boolean success = scanner.wifiManager.startScan();

        if (!success) {
            Log.w(LOGTAG, "startScan failed");
            scanner.onScanFailure();
        }
    }

    private Scanner(Activity activity, NetworkNamesCallback callback, Runnable failureCallback) {
        unityActivity = activity;
        wifiManager = (WifiManager)
                unityActivity.getApplicationContext()
                        .getSystemService(Context.WIFI_SERVICE);
        this.callback = callback;
        this.failureCallback = failureCallback;

        // register callback for wifi scan results
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context c, Intent intent) {
                boolean success = intent.getBooleanExtra(
                        WifiManager.EXTRA_RESULTS_UPDATED, false);
                if (success) {
                    onScanSuccess();
                } else {
                    // scan failure handling
                    onScanFailure();
                }

                cleanup();
            }
        };

        unityActivity.getApplicationContext().registerReceiver(receiver, intentFilter);
    }

    private void cleanup() {
        unityActivity.getApplicationContext().unregisterReceiver(receiver);
    }

    private void onScanSuccess() {
        List<ScanResult> results = wifiManager.getScanResults();
        Log.d(LOGTAG, "Number of scan results: "+Integer.toString(results.size()));

        for(ScanResult result : results) {
            Log.d(LOGTAG, "Scan result: "+result.SSID);
        }

        List<String> ssids = new ArrayList<>();
        for (ScanResult result : results) {
            ssids.add(result.SSID);
        }

        callback.OnResult(ssids.toArray(new String[]{}));
    }

    private void onScanFailure() {
        if (failureCallback != null)
            failureCallback.run();
    }
}