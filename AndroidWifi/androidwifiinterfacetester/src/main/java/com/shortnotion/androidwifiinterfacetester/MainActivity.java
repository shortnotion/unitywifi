package com.shortnotion.androidwifiinterfacetester;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Debug;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;

import com.shortnotion.androidwifiinterface.UnityPlugin;

public class MainActivity extends AppCompatActivity {
    UnityPlugin plugin;
    private TextView logTextView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        log("onCreate");
        plugin = UnityPlugin.create(this);

        Button button = findViewById(R.id.scanButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                log("scanButton#onClick");

                plugin.GetAvailableNetworks((String[] configuredSsids, String[] unconfiguredSsids) -> {
                    log("-- Scan results --");
                    for (String r : configuredSsids)
                        log(" - Configured NETWORK: " + r);
                    for (String r : unconfiguredSsids)
                        log(" - Unconfigured NETWORK: " + r);

                    log("\n");
                });
            }
        });

        button = findViewById(R.id.connInfo);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                log("connInfo#onClick");
                plugin.GetConnectionInfo((boolean enabled, boolean connected, String ssid) -> {
                    log("Connection info (enabled, connected, ssid): "
                            +Boolean.toString(enabled)+" "+Boolean.toString(connected)+" "+ssid);
                });
            }
        });

        button = findViewById(R.id.btnConnect);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText ssidView = findViewById(R.id.ssidView);
                EditText pswView = findViewById(R.id.pswView);

                String ssid = ssidView.getText().toString();
                String psw = pswView.getText().toString();
                log("connect: " + psw + "@" + ssid);

                plugin.ConfigureNetwork(ssid, psw);
            }
        });

        button = findViewById(R.id.btnConnectExisting);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText ssidView = findViewById(R.id.ssidView);
                String ssid = ssidView.getText().toString();
                log("connect existing: " + ssid);
                plugin.Connect(ssid);
            }
        });
    }

    private void log(String msg) {
        if (logTextView == null)
            logTextView = findViewById(R.id.logTextView);

        String txt = logTextView.getText().toString();
        txt += "\n"+msg;

        logTextView.setText(txt);
        Log.i("LOG", msg);
    }
}
