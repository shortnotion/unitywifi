# UnityWifi

This repository contains an Android plugin for Unity to access and change wifi settings from inside Unity.

## AndroidWifi

The ```AndroidWifi``` folder contains an Android Studio project with the ```AndroidWifiInterface``` library that produces the ```AndroidWifiInterface-release.aar``` that can be used in Unity projects.

There's also the ```androidwifiinterfacetester``` Native android app for debugging and manually testing the functionality of the plugin.

## AndroidWifiTest

The ```AndroidWifiTest``` folder contains a Unity project with scripts that provide a C-Sharp API for the plugin and an example scene/script that shows how to use the plugin.


![screenshot](screenshot.jpeg)
_Example scene_