﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif

namespace AndroidWifi {
	public class Plugin
	{
		public static void CheckPermissions() {
			#if PLATFORM_ANDROID
			if (!Permission.HasUserAuthorizedPermission(Permission.FineLocation)) // CoarseLocation
					Permission.RequestUserPermission(Permission.FineLocation); // CoarseLocation
			#endif
		}
		#region Singleton

		private static Plugin singletonInstance = null;

		public static Plugin Instance { get {
			if (singletonInstance == null) singletonInstance = new Plugin();
			return singletonInstance;
		}}

		#endregion
	
		#region AndroidJava Bridge
		#if UNITY_ANDROID

			const string pluginClassName = "com.shortnotion.androidwifiinterface.UnityPlugin";
			// const string callbackClassName = pluginClassName+"$PublishCallback";
			const string connectionCallbackClassName = pluginClassName+"$ConnectionCallback";
			const string networksCallbackClassName = pluginClassName+"$NetworksCallback";

			AndroidJavaObject _pluginInstance = null;

			/// Returns a singleton instance of the Android Plugin class
			public AndroidJavaObject AndroidPluginInstance { get {
				if (_pluginInstance == null) {
					_pluginInstance = new AndroidJavaClass(pluginClassName).CallStatic<AndroidJavaObject>("create", CurrentActivity);
				}

				return _pluginInstance;
			} }

			/// Returns the Unity Player's main android activity
			private AndroidJavaObject CurrentActivity { get {
				// https://answers.unity.com/questions/59622/accessing-the-activity-context-in-android-plugin.html
				AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer"); 
				return jc.GetStatic<AndroidJavaObject>("currentActivity");
			}}

      /// Callback proxy class to receive async connection info results
			class ConnectionInfoCallback : AndroidJavaProxy { 
				private System.Action<bool, bool, string> handler;
				private bool removeQuotes = true;

				public ConnectionInfoCallback(System.Action<bool, bool, string> callback) : base(connectionCallbackClassName) {
					handler = callback;
				}

				public void OnResult(bool enabled, bool connected, string ssid) {
					handler.Invoke(enabled, connected, removeQuotes && ssid != null ? NetworksCallback.Unquoted(ssid) : ssid);
				}
			}

      /// Callback proxy class to receive async networks info results
			class NetworksCallback : AndroidJavaProxy { 
				private System.Action<string[], string[]> handler;
				private bool removeQuotes = true;
				private bool rejectBlank = true;

				public NetworksCallback(System.Action<string[], string[]> callback) : base(networksCallbackClassName) {
					handler = callback;
				}

				public void OnResult(string[] configuredSSIDS, string[] unconfiguredSSIDS) {
					if (removeQuotes) {
						configuredSSIDS = configuredSSIDS
							.Select(ssid => Unquoted(ssid))
							.Select(ssid => ssid.Trim())
							.Where(ssid => rejectBlank ? !string.IsNullOrEmpty(ssid) : true)
							.ToArray();

						unconfiguredSSIDS = unconfiguredSSIDS
							.Select(ssid => Unquoted(ssid))
							.Select(ssid => ssid.Trim())
							.Where(ssid => rejectBlank ? !string.IsNullOrEmpty(ssid) : true)
							.ToArray();
					}

					handler.Invoke(configuredSSIDS, unconfiguredSSIDS);
				}

				public static string Unquoted(string ssid) {
					if (ssid.StartsWith("\"") && ssid.EndsWith("\"")) {
						ssid = ssid.Remove(0, 1).Remove(ssid.Length-1, 1);
					} 

					return ssid;
				}
			}

		#endif
		#endregion       

    #region API
      public bool GetAvailableNetworks(System.Action<string[], string[]> callback) {

				#if UNITY_ANDROID
				if (Application.platform == RuntimePlatform.Android) {
					Plugin.CheckPermissions();
					AndroidPluginInstance.Call("GetAvailableNetworks", new NetworksCallback(callback));
					return true;
				}
				#endif
	
				Debug.LogWarning("Platform not supported by AndroidWifi plugin: "+Application.platform);
				callback.Invoke(new string[] { }, new string[] { "dummy network for testing #1", "dummy network for testing #2", "dummy network for testing #3" });
				return false;
			}				

			public bool GetConnectionInfo(System.Action<bool, bool, string> callback) {
				#if UNITY_ANDROID
				if (Application.platform == RuntimePlatform.Android) {
					Plugin.CheckPermissions();
					AndroidPluginInstance.Call("GetConnectionInfo", new ConnectionInfoCallback(callback));
					return true;
				}
				#endif
			
				Debug.LogWarning("Platform not supported by AndroidWifi plugin: "+Application.platform);
				callback.Invoke(false, false, null);
				return false;
			}

			public bool GetConfiguredNetworks(System.Action<string[]> callback) {
				#if UNITY_ANDROID
				if (Application.platform == RuntimePlatform.Android) {
					Plugin.CheckPermissions();
					AndroidPluginInstance.Call("GetConfiguredNetworks", new NetworksCallback(
						(string[] a, string[] b) => callback.Invoke(a)));
					return true;
				}
				#endif

				Debug.LogWarning("Platform not supported by AndroidWifi plugin: "+Application.platform);
				callback.Invoke(new string[]{});
				return false;
			}

			public bool ConfigureNetwork(string ssid, string password) {
				#if UNITY_ANDROID
				if (Application.platform == RuntimePlatform.Android) {
					Plugin.CheckPermissions();
					Debug.Log("Plugin.ConfigureNetwork with: "+ssid);
					AndroidPluginInstance.Call("ConfigureNetwork", ssid, password);
					return true;
				}
				#endif

				Debug.LogWarning("Platform not supported by AndroidWifi plugin: "+Application.platform);
				return false;
			}

			public bool RemoveNetwork(string ssid) {
				#if UNITY_ANDROID
				if (Application.platform == RuntimePlatform.Android) {
					Plugin.CheckPermissions();
					AndroidPluginInstance.Call("RemoveNetwork", ssid);
					return true;
				}
				#endif

				Debug.LogWarning("Platform not supported by AndroidWifi plugin: "+Application.platform);
				return false;
			}


			public bool Connect(string ssid) {
				#if UNITY_ANDROID
				if (Application.platform == RuntimePlatform.Android) {
					Plugin.CheckPermissions();
					Debug.Log("Plugin.Connect with: "+ssid);
					AndroidPluginInstance.Call("Connect", ssid);
					return true;
				}
				#endif

				Debug.LogWarning("Platform not supported by AndroidWifi plugin: "+Application.platform);
				return false;
			}
    #endregion
	}
}