﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace AndroidWifi {

    public class AndroidWifi : MonoBehaviour
    {
        [Tooltip("Checks, and if necessary requests, location permissions in the Start() method.")]
        public bool CheckPermissions = true;
        [Tooltip("When true will look for the current wifi connection status and either invoke the OnConnection or OnNoConnection event")]
        public bool CheckConnectionAtStart = true;

        [Tooltip("Time after which a Connect attempt should be considered failed if the connection hasn't been established yet")]
        public float ConnectTimeout = 8.0f;
        [Tooltip("Interval (in seconds) at which to check if a connection has been established")]
        public float ConnectCheckInterval = 1.0f;
        [Tooltip("Time after which a Configure attempt should be considered failed if the network isn't configured yet")]
        public float ConfigureTimeout = 5.0f;
        [Tooltip("Interval (in seconds) at which to check if a network has been configured")]
        public float ConfigureCheckInterval = 1.0f;

        public UnityEvent OnConnection = new UnityEvent();
        public UnityEvent OnNoConnection = new UnityEvent();

        public class State {
            public string[] ConnectableNetworks;
            public string[] ConfigurableNetworks;
            public bool WifiEnabled;
            public bool WifiConnected;
            public string CurrentNetwork;
        }

        public State Info = new State();


        private Queue<System.Action> updateActions = new Queue<System.Action>();
        private GameObject dialog;

        void Start ()
        {
            if (CheckPermissions)
                Plugin.CheckPermissions();

            if (CheckConnectionAtStart) {
                GetConnectionInfo(null);
            }
        }

        // Update is called once per frame
        void Update()
        {
            foreach(var func in updateActions)
                func.Invoke();
            updateActions.Clear();
        }

        #region Plugin API
        public void GetAvailableNetworks(System.Action<string[], string[]> callback) {
            Plugin.Instance.GetAvailableNetworks((string[] connectable, string[] configurable) => {
                Info.ConnectableNetworks = connectable;
                Info.ConfigurableNetworks = configurable;

                nextUpdate(() => {
                    callback.Invoke(connectable, configurable);
                });
            });
        }

        public void GetConnectionInfo(System.Action<bool, bool, string> callback) {
            Plugin.Instance.GetConnectionInfo((bool enabled, bool connected, string ssid) => {
                Info.WifiEnabled = enabled;
                Info.WifiConnected = connected;
                Info.CurrentNetwork = ssid;

                if (callback != null)
                    nextUpdate(() => callback.Invoke(enabled, connected, ssid));

                var e = connected ? OnConnection : OnNoConnection;
                e.Invoke();
            });
        }

        public bool Connect(string ssid, System.Action<bool> callback = null) {
            return Connect(ssid, callback, ConnectTimeout);
        }

        public bool Connect(string ssid, System.Action<bool> callback, float timeout) {
            bool result = Plugin.Instance.Connect(ssid);

            if (callback != null) {
                StartCoroutine(ConnectCallback(ssid, callback, timeout));
            }

            return result;
        }

        public bool ConfigureNetwork(string ssid, string password, System.Action<bool> callback = null) {
            return ConfigureNetwork(ssid, password, callback, ConfigureTimeout);
        }

        public bool ConfigureNetwork(string ssid, string password, System.Action<bool> callback, float timeout) {
            bool result = Plugin.Instance.ConfigureNetwork(ssid, password);

            if (callback != null) {
                StartCoroutine(ConfigureCallback(ssid, callback, timeout));
            }

            return result;
        }

        public bool ConfigureAndConnect(string ssid, string password, System.Action<bool> callback = null) {
            // Configure network (save password for specified network)
            return ConfigureNetwork(ssid, password, (bool configureSuccess) => {
                if (!configureSuccess) {
                    // Invoke caller's callback with failure result
                    callback.Invoke(false);
                    return;
                }

                // If network is succesffully configured, try to connect to it
                Connect(ssid, (bool success) => {
                   if (!success) {
                       // If connection fails, UNconfigure network,
                       // assuming it was the wrong password
                       Plugin.Instance.RemoveNetwork(ssid);
                   }

                    // Invoke caller's callback with result
                   callback.Invoke(success);
                });
            });
        }
        #endregion

        #region Public Helper Methods
        public bool IsConnectable(string ssid) {
            return Array.IndexOf(Info.ConnectableNetworks, ssid) != -1;
        }
        #endregion

        private IEnumerator ConnectCallback(string ssid, System.Action<bool> callback, float timeout) {
            yield return TimeoutCallback(callback, timeout, ConnectCheckInterval, (confirm) => {
                // async verification test
                GetConnectionInfo((bool enabled, bool connected, string connectedSsid) => {
                    if (enabled && connected && connectedSsid.Equals(ssid))
                        confirm.Invoke();
                });
            });
        }

        private IEnumerator ConfigureCallback(string ssid, System.Action<bool> callback, float timeout) {
            System.Action<bool> callbackWrapper = (bool result) => {
                if (!result) {
                    // remove network
                }

                callback.Invoke(result);
            };
            yield return TimeoutCallback(callbackWrapper, timeout, ConfigureCheckInterval, (confirm) => 
                // async verification test
                Plugin.Instance.GetConfiguredNetworks((string[] ssids) => {
                    if (Array.IndexOf(ssids, ssid) != -1)
                        confirm.Invoke();
                }));
        }

        private IEnumerator TimeoutCallback(System.Action<bool> callback, float timeout, float checkInterval, System.Action<System.Action> test) {
            var deadline = Time.time + timeout;
            var isConfirmed = false;

            while(Time.time < deadline && !isConfirmed) {
                // wait a bit
                yield return new WaitForSeconds(checkInterval);
                // perform test
                test.Invoke(() => {
                    // test succeeeded; let caller know
                    callback.Invoke(true);
                    isConfirmed = true;
                });
            }

            if (!isConfirmed)
                callback.Invoke(false);
        }

        private void nextUpdate(System.Action func) {
            updateActions.Enqueue(func);
        }
    }
}