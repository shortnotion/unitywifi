﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AndroidWifi.Example {
  public class ExampleLogic : MonoBehaviour {
    public AndroidWifi AndroidWifi;

    public UnityEngine.UI.Dropdown NetworksDropdown;
    public UnityEngine.UI.InputField NameInput;
    public UnityEngine.UI.InputField PasswordInput;
    public UnityEngine.UI.Text StatusText;

    private string[] selectableNetworks = new string[] {};

    [System.Serializable]
    public class UiTexts {
      public string SearchingForNetworks = "Looking for networks...";
      public string Disabled = "Wifi DISABLED";
      public string Disconnected = "DISCONNECTED";
      public string SelectNetwork = "Select a network...";

      [Tooltip("Use the {{name}} macro to have the name of the network inserted in the text")]
      public string ConnectingTo = "Connecting to: {{name}}...";
      [Tooltip("Use the {{name}} macro to have the name of the network inserted in the text")]
      public string FailedToConnectTo = "Failed to connect to: {{name}}";
      [Tooltip("Use the {{name}} macro to have the name of the network inserted in the text")]
      public string ConnectedTo = "Connected to: {{name}}";
      public string NoName = "No name selected or entered";


      public string GetConnectingTo(string name) {
        return ConnectingTo.Replace("{{name}}", name);
      }
      public string GetFailedToConnectTo(string name) {
        return FailedToConnectTo.Replace("{{name}}", name);
      }
      public string GetConnectedTo(string name) {
        return ConnectedTo.Replace("{{name}}", name);
      }
    }

    public UiTexts Texts = new UiTexts();


    void Start() {
      NetworksDropdown.onValueChanged.AddListener((idx) => {
        if (idx == 0) NameInput.text = "";
        else NameInput.text = selectableNetworks[idx-1];
      });

      setStatus(Texts.SearchingForNetworks);
      FetchConnectionInfo();
      FetchNetworks();
    }

    /// Scans for available networks and updates NetworksDropdown UI
    public void FetchNetworks() {
      // Perform async load request
      AndroidWifi.GetAvailableNetworks((string[] connectable, string[] configurable) => {
        // update UI
        string[] ssids = connectable.Concat(configurable).ToArray();
        Array.Sort(ssids);
        setSelectableNetworks(ssids);
      });
    }

    /// Requests current connection information and updates the status in the UI
    public void FetchConnectionInfo() {
      // Perform async connection info request
      AndroidWifi.GetConnectionInfo((bool enabled, bool connected, string ssid) => {
        if (!enabled) {
          setStatus(Texts.Disabled);
          return;
        }

        if (!connected) {
          setStatus(Texts.Disconnected);
          return;
        }

        setStatus(Texts.GetConnectedTo(ssid));
      });
    }

    /// Connect to network
    /// (configuring the network with currently entered password first if needed)
    public void Connect() {
      var ssid = NameInput.text;
      var psw = PasswordInput.text;

      if (string.IsNullOrEmpty(ssid)) {
        setStatus(Texts.NoName);
        return;
      }

      // Update UI
      setStatus(Texts.GetConnectingTo(ssid));

      // one callback that can be used for both Connect and ConfigureAndConnect
      System.Action<bool> callback = (bool result) => {
        // show result
        if (result)
          setStatus(Texts.GetConnectedTo(ssid));
        else
          setStatus(Texts.GetFailedToConnectTo(ssid));

        // reload network and connectin status, just to double-check
        FetchConnectionInfo();
        FetchNetworks();
      };

      // No password; just try to connect directly
      if (string.IsNullOrEmpty(psw)) {
        AndroidWifi.Connect(ssid, callback);
        return;
      }

      // Configure with password and THEN connect
      AndroidWifi.ConfigureAndConnect(ssid, psw, callback);
    }

    /// Updates NetworksDropdown with the given options,
    /// tries to make same network stays selected (if still available)
    private void setSelectableNetworks(string[] ssids) {
      // store currently selected network name
      string selected = null;

      if (selectableNetworks.Length > 0 && NetworksDropdown.value > 0)
        selected = selectableNetworks[NetworksDropdown.value];

      // update selectable networks with given list of network names
      selectableNetworks = ssids;

      var options = new string[]{ Texts.SelectNetwork }.Concat(selectableNetworks).ToArray();

      // update DropDown options
      NetworksDropdown.ClearOptions();
      foreach(var ssid in options) {
        var opt = new Dropdown.OptionData();
        opt.text = ssid;
        NetworksDropdown.options.Add(opt);
      }

      if (selected != null) {
        NetworksDropdown.value = 0;
        return;
      } 
      
      // re-activated previously selected network name if (still) available
      int idx = Array.IndexOf(selectableNetworks, selected);
      if (idx > -1 && (idx+1) < selectableNetworks.Length)
        NetworksDropdown.value = idx+1;
    }

    /// helpers method for setting the StatusText text
    private void setStatus(string status) {
      StatusText.text = status;
    }
  }
}